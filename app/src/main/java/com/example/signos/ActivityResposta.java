package com.example.signos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class ActivityResposta extends Activity {


    private TextView TextResposta;
    private TextView Subtitulo;
    private ImageButton BotaoVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resposta);


        TextResposta = findViewById(R.id.RespostaText);
        Subtitulo = findViewById(R.id.SubtituloID);
        BotaoVoltar = findViewById(R.id.BotaoID);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String BuscarResposta = extras.getString("codigo");
            TextResposta.setText(BuscarResposta);
            String BuscarSubtitulo = extras.getString("subtitulo");
            Subtitulo.setText(BuscarSubtitulo);
        }
        BotaoVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}

